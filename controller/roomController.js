const Rooms = require('../models/roomdb')

exports.add_new_room = async (req, res) => {
  const room = new Rooms({
    nameId: req.body.nameId, //String
    size: req.body.size,//String
    amount: Number(req.body.amount),//Number
    price: Number(req.body.price),//Number
    status: Number(req.body.status)//Number
  })
  const addedRoom = await room.save()
  res.json(addedRoom)

}

exports.show_all_room = async (req, res) => {
  const showroom = await Rooms.find()
  res.json(showroom)
}

exports.search_roomById = async (req, res) => {
  const searchRoom = await Rooms.findById(req.params.id)
  res.json(searchRoom)
}

exports.remove_roomById = async (req, res) => {
  const removeRoom = await Rooms.remove({ _id: req.params.id })
  res.json(removeRoom);

}

exports.update_roomById = async (req, res) => {
  const updateRoom = await Rooms.updateOne({ _id: req.params.id }, {
    $set: {
      nameId:req.body.nameId,
      size: req.body.size,
      price: req.params.price,
      status: req.params.status
    }
  })
  res.json(updateRoom)
}