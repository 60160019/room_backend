const mongoose = require('mongoose')

const RoomSchema = mongoose.Schema({
  nameId: String,
  size: String,
  price: Number,
  status: Number
});

module.exports = mongoose.model('Room', RoomSchema);