const express = require('express');
const router = express.Router();
const roomController = require('../controller/roomController')


router.get('/', roomController.show_all_room);
router.get('/:id', roomController.search_roomById);
router.post('/add', roomController.add_new_room);
router.delete('/remove/:id', roomController.remove_roomById);
router.patch('/update/:id',roomController.update_roomById);

module.exports = router;